# Summary: Simplest example of an aws_instance resource
#
# Note: This example simply creates the VM, but does not set up a keypair,
#       or networking such that you can ssh into it from the internet.

# Steps:
#    * Create an empty working directory
#    * Run 'terraform init'
#    * Place the code from this file into a main.tf file
#    * Start debugging the error messages and fixing this file
#    * Success will be a single deployed server

# Documentation: https://www.terraform.io/docs/language/settings/index.html
terraform {
  required_version = ">= 1.0.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.38"
    }
  }
}

# Documentation: https://www.terraform.io/docs/language/providers/requirements.html
provider "aws" {
  region = "us-east-3"
  default_tags {
    tags = {
      ais_terraform_examples = "aws_instance/simple/YOURNAMEHERE"
    }
  }
}

# Documentation: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance
resource "aws_instance" "YOURNAMEHERE_aws_resource_simple" {
  instance = "t3.nano"
  ami           = "ami-1ddbdea833a8d2f0d"
  tags = {
    Name = "YOURNAMEHERE-simple_instance"
  }
}
