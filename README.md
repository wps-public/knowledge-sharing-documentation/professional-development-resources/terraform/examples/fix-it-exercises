# fix-it-exercises

Exercises to fix what is broken

See what kinds of errors you can find and fix with the following:
- `terraform validate`
- `terraform plan`
- `terraform apply`

To avoid namespace collisions, there are commonly things like 'YOURNAMEHERE' that you should replace with your name.

Remember to run this once you are done for the day:
- `terraform destroy`

Thanks!
